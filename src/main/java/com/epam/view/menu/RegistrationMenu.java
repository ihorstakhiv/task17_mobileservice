package com.epam.view.menu;

import com.epam.controller.Controller;
import com.epam.controller.RegisterMenuController;
import com.epam.utils.constant.MenuConstant;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class RegistrationMenu {

    public static final Logger logger = LogManager.getLogger(RegistrationMenu.class);
    private Map<Integer, String> registerMenu;
    private Map<Integer, Controller> registerMethodMenu;

    RegistrationMenu() {
        registerMenu = new HashMap<>();
        registerMethodMenu = new HashMap<>();

        registerMenu.put(1, "Enter login and password");
        registerMenu.put(2, "Exit");

        registerMethodMenu.put(1, new RegisterMenuController()::createUser);
        registerMethodMenu.put(2, new RegisterMenuController()::back);

    }

    void outputSubRegisterMenu() {
        Scanner scanner = new Scanner(System.in);
        int key = MenuConstant.getKey();
        do {
            print();
            logger.info("Please make your choice: ");
            try {
                key = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                logger.error(e.toString());
                continue;
            }
            if (!this.registerMethodMenu.containsKey(key)) {
                logger.error("Invalid choose !" + "\nPlease try again : ");
                continue;
            }
            this.registerMethodMenu.get(key).print();
        } while (key != MenuConstant.getMaxLoginMenuValue());
    }

    public void print() {
        registerMenu.forEach((k, v) -> logger.info(k + " - " + v));
    }
}
