package com.epam.view.menu;

import com.epam.controller.Controller;
import com.epam.controller.LoginMenuController;
import com.epam.utils.constant.MenuConstant;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class LoginMenu {
    public static final Logger logger = LogManager.getLogger(RegistrationMenu.class);
    private Map<Integer, String> loginMenu;
    private Map<Integer, Controller> loginMethodMenu;

    LoginMenu() {
        loginMenu = new HashMap<>();
        loginMethodMenu = new HashMap<>();

        loginMenu.put(1, "Enter login and password");
        loginMenu.put(2, "Exit");

        loginMethodMenu.put(1, new LoginMenuController()::provideUserCredentials);
        loginMethodMenu.put(2, new LoginMenuController()::back);
    }

    void outputSubLoginMenu() {
        Scanner scanner = new Scanner(System.in);
        int key = MenuConstant.getKey();
        do {
            print();
            logger.info("Please make your choice: ");
            try {
                key = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                logger.error(e.toString());
                continue;
            }
            if (!this.loginMethodMenu.containsKey(key)) {
                logger.error("Invalid choose !" + "\nPlease try again : ");
                continue;
            }
            this.loginMethodMenu.get(key).print();
        } while (key != MenuConstant.getMaxLoginMenuValue());
    }

    public void print() {
        loginMenu.forEach((k, v) -> logger.info(k + " - " + v));
    }
}
