package com.epam.view;

import com.epam.utils.constant.MenuConstant;
import com.epam.view.menu.WelcomeMenu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.Scanner;

public class View {
    public static final Logger logger = LogManager.getLogger(View.class);
    private Scanner scanner;
    private int key;

    public View() {
        scanner = new Scanner(System.in);
    }

    public void start() {
        do {
            new WelcomeMenu().print();
            logger.info("Please make your choice : ");
            try {
                key = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                logger.error(e.toString());
                continue;
            }
            if (!new WelcomeMenu().welcomeMethodMenu.containsKey(key)) {
                logger.info("Invalid chose,please try again!");
                continue;
            }
            new WelcomeMenu().welcomeMethodMenu.get(key).print();
        } while (key != MenuConstant.getMaxWelcomeMenuValue());
    }
}
