package com.epam.controller;

@FunctionalInterface
public interface Controller{
    void print();
}
