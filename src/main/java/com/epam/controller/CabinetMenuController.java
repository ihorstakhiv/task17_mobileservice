package com.epam.controller;

import com.epam.dao.UserCabinetDao;
import com.epam.model.Call;
import com.epam.model.Message;
import com.epam.model.User;
import com.epam.service.OperatorService;
import com.epam.service.PhoneService;
import com.epam.service.UserCabinetService;
import com.epam.utils.TimeCounter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CabinetMenuController implements com.epam.controller.Controller {
    public static final Logger logger = LogManager.getLogger(WelcomeMenuController.class);
    private User user;
    private Scanner scanner;

    public CabinetMenuController(User user) {
        scanner = new Scanner(System.in);
        this.user = user;
    }

    public void getAccountInfo() {
        user.print();
        try {
            new OperatorService().getNetworkProvider(user).print();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void showBalance() {
        logger.info(String.format("Your balance - " + new UserCabinetService().getBalance(user) + " UAH\n"));
    }

    public void addMoneyToBalance() {
        logger.info("How much money you want to add?" + "\n" + "Please enter :");
        double amountToAdd = scanner.nextDouble();
        new UserCabinetService().refillBalance(user, amountToAdd);
        logger.info(String.format("You have successfully updated your balance for %s UAH", Double.toString(amountToAdd)));
    }

    public void makeCall() {
        String endCall = "";
        int callTimeInMin;
        String numberToCall = "";
        double callPrice = 0;
        if (new UserCabinetDao().getBalance(user) > 0) {
            logger.info("Enter phone number to call (in format: 0XXXXXXXXX): ");
            numberToCall = scanner.nextLine();
            int phoneNumberNetworkProviderID = new OperatorService().detectNetworkProvider(numberToCall);
            try {
                if (phoneNumberNetworkProviderID == new OperatorService().getNetworkProvider(user).getId()) {
                    callPrice = new OperatorService().getNetworkProvider(user).getCall_in_network_price();
                } else {
                    callPrice = new OperatorService().getNetworkProvider(user).getCall_out_network_price();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (new UserCabinetService().getBalance(user) > callPrice) {
                do {
                    TimeCounter timeCounter = new TimeCounter();
                    double maxTimeOfCall = Math.round(new UserCabinetService().getBalance(user) / callPrice);
                    logger.info("balance: " + new UserCabinetService().getBalance(user) + ";  call price: " + callPrice + ";  " + "max time of call: " + maxTimeOfCall);
                    while (!endCall.equals("end")) {
                        logger.info("Enter 'end' to finish your call\n");
                        endCall = scanner.nextLine();
                        if (!endCall.equals("end")) {
                            logger.error("Incorrect data");
                        }
                    }
                    callTimeInMin = (int) (timeCounter.getTime() / 60);
                    if (timeCounter.getTime() % 60 != 0) {
                        callTimeInMin = callTimeInMin + 1;
                    }
                } while (!endCall.equals("end"));
                logger.debug("End call");
                logger.debug("Your call lasted " + callTimeInMin + " min.\n");
                double amountToDeduct = callTimeInMin * callPrice;
                int rowsUpdated = new UserCabinetService().deductBalance(user, amountToDeduct);
                int rowsUpdated1 = new PhoneService().registerCall(numberToCall, callTimeInMin, user.getId());
            } else {
                logger.error("Insufficient funds. Please update your balance\n");
            }
        } else {
            logger.error("Insufficient funds. Please update your balance\n");
        }
    }

    public void getCallHistory() {
        List<Call> calls = new ArrayList<>();
        try {
            calls = new PhoneService().getCallHistory(user.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        logger.debug("LIST OF CALLS: ");
        calls.forEach(c -> c.print());
    }

    public void sendMessage() {
        Scanner scanner = new Scanner(System.in);
        logger.info("Enter phone number to send Message to (in format: 0XXXXXXXXX): ");
        String receiverNumber = scanner.nextLine();
        double smsPrice = 0;
        int smsCount = 0;
        int phoneNumberNetworkProviderID = new OperatorService().detectNetworkProvider(receiverNumber);
        try {
            if (phoneNumberNetworkProviderID == new OperatorService().getNetworkProvider(user).getId()) {
                smsPrice = new OperatorService().getNetworkProvider(user).getSms_in_network_price();
            } else {
                smsPrice = new OperatorService().getNetworkProvider(user).getSms_out_network_price();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        logger.info("Enter Message body text: ");
        String smsText = scanner.nextLine();
        try {
            if (smsText.length() % new OperatorService().getNetworkProvider(user).getChars_in_sms() == 0) {
                smsCount = smsText.length() / new OperatorService().getNetworkProvider(user).getChars_in_sms();
            } else {
                smsCount = smsText.length() / new OperatorService().getNetworkProvider(user).getChars_in_sms() + 1;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        double amountToDeduct = smsCount * smsPrice;
        if (amountToDeduct <= new UserCabinetService().getBalance(user)) {
            int rowsUpdaetd = new UserCabinetService().deductBalance(user, amountToDeduct);
            int rowsUpdated1 = new PhoneService().registerSMS(receiverNumber, smsText, user.getId());
            logger.info("Your message is sent.");
        } else {
            logger.error("Insufficient money. Please update your balance ! \n");
        }
    }

    public void getMessageHistory() {
        List<Message> messages = new ArrayList<>();
        try {
            messages = new PhoneService().getSmsHistory(user.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
        logger.info("LIST OF Message: ");
        messages.forEach(s -> s.print());
    }

    public void exit() {
        logger.trace("Back to Login menu\n");
    }

    @Override
    public void print() {
    }
}
