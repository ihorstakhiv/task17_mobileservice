package com.epam.service;

import com.epam.dao.LoginDao;
import com.epam.model.User;
import java.sql.SQLException;

public class LoginService {
    public User getUser(String username, String password) throws SQLException {
        return new LoginDao().getUser(username,password);
    }
}
