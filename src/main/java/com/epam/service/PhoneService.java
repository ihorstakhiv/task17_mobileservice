package com.epam.service;

import com.epam.dao.PhoneDao;
import com.epam.model.Call;
import com.epam.model.Message;
import java.sql.SQLException;
import java.util.List;

public class PhoneService {
    public int registerCall(String receiver, int callTime, int userID) {
        return new PhoneDao().registerCall(receiver, callTime, userID);
    }

    public List<Call> getCallHistory(int userID) throws SQLException {
        return new PhoneDao().getCallHistory(userID);
    }

    public int registerSMS(String receiverNumber, String smsText, int userID) {
        return new PhoneDao().registerSMS(receiverNumber, smsText, userID);
    }

    public List<Message> getSmsHistory(int userID) throws SQLException {
        return new PhoneDao().getSmsHistory(userID);
    }
}
