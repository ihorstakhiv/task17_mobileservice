package com.epam;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.epam.view.View;

public class Application {
    public static final Logger logger = LogManager.getLogger(Application.class);
    public static void main(String[] args){
        new View().start();
    }
}
