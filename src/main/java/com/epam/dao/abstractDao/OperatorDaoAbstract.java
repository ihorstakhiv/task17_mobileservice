package com.epam.dao.abstractDao;

import com.epam.model.Operator;
import com.epam.model.User;

import java.sql.SQLException;

public interface OperatorDaoAbstract {
     Operator getNetworkProvider(User user) throws SQLException;
     int detectNetworkProvider(String number);
}
