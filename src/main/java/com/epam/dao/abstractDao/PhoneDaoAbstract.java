package com.epam.dao.abstractDao;

import com.epam.model.Call;
import com.epam.model.Message;

import java.sql.SQLException;
import java.util.List;

public interface PhoneDaoAbstract {
    int registerCall(String receiver, int callTime, int userID);
    List<Call> getCallHistory(int userID) throws SQLException;
    int registerSMS(String receiverNumber, String smsText, int userID);
    List<Message> getSmsHistory(int userID) throws SQLException;
}
