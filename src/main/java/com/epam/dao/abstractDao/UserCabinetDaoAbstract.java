package com.epam.dao.abstractDao;

import com.epam.model.User;

import java.sql.SQLException;

public interface UserCabinetDaoAbstract {
    double getBalance(User user);
    int refillBalance(User user, double amountToAdd);
    int updateBalance(User user, double newBalance);
    int minusBalance(User user, double amoutToDeduct);
    int getPhoneID(User user) throws SQLException;
}
