package com.epam.dao;

import com.epam.dao.abstractDao.LoginDaoAbstract;
import com.epam.database.DatabaseConnector;
import com.epam.utils.DatabaseParser;
import com.epam.model.User;
import com.epam.utils.PropertiesReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LoginDao implements LoginDaoAbstract {

    @Override
    public User getUser(String username, String password) throws SQLException {
        Connection connection = DatabaseConnector.getDBConnection();
        ResultSet rs = null;
        User user = null;
        try {
            PreparedStatement ps = connection.prepareStatement(PropertiesReader.getStringFromProperties("GET_USER"));
            ps.setString(1, username);
            ps.setString(2, password);
            rs = ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        while (rs.next()) {
            user = (User) new DatabaseParser(User.class).fromResultSetToEntity(rs);
        }
        return user;
    }
}
