package com.epam.dao;

import com.epam.dao.abstractDao.OperatorDaoAbstract;
import com.epam.database.DatabaseConnector;
import com.epam.utils.DatabaseParser;
import com.epam.model.Operator;
import com.epam.model.User;
import com.epam.utils.PropertiesReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OperatorDao implements OperatorDaoAbstract {
    public static final Logger logger = LogManager.getLogger(OperatorDao.class);

    @Override
    public Operator getNetworkProvider(User user) throws SQLException {
        Operator operator = null;
        Connection connection = DatabaseConnector.getDBConnection();
        ResultSet rs = null;
        try {
            PreparedStatement ps = connection.prepareStatement(PropertiesReader.getStringFromProperties("GET_PROVIDER"));
            ps.setInt(1, user.getId());
            rs = ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        while (rs.next()) {
            operator = (Operator) new DatabaseParser(Operator.class).fromResultSetToEntity(rs);
        }
        return operator;
    }
    @Override
    public int detectNetworkProvider(String number) {
        String kyivstarPhoneNumberPattern = "(^[0])+([6|9])+([6|7|8])+ *\\d{7}";
        String lifePhoneNumberPattern = "(^[0])+([6|7|9])+([3])+ *\\d{7}";
        Pattern p1 = Pattern.compile(kyivstarPhoneNumberPattern);
        Pattern p2 = Pattern.compile(lifePhoneNumberPattern);
        Matcher matcher1 = p1.matcher(number);
        Matcher matcher2 = p2.matcher(number);
        if (matcher1.find()) {
            logger.trace("Your network provider is Kyivstar\n");
            return 1;
        } else if (matcher2.find()) {
            logger.trace("Your network provider is Life\n");
            return 2;
        } else {
            logger.trace("Your network provider is Unknown\n");
            return 3;
        }
    }
}
