package com.epam.dao;

import com.epam.dao.abstractDao.RegisterDaoAbstract;
import com.epam.database.DatabaseConnector;
import com.epam.utils.PropertiesReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class RegisterDao implements RegisterDaoAbstract {

    @Override
    public int createUser(String name, String surname, String username, String password) {
        Connection connection = DatabaseConnector.getDBConnection();
        int countOfRowsUpdated = 0;
        try {
            PreparedStatement ps = connection.prepareStatement(PropertiesReader.getStringFromProperties("CREATE_USER"));
            ps.setString(1, name);
            ps.setString(2, surname);
            ps.setString(3, username);
            ps.setString(4, password);
            countOfRowsUpdated = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return countOfRowsUpdated;
    }

    @Override
    public int createPhone(String name, String surname, String username, String password, String model, String number, int networkProviderId) {
        Connection connection = DatabaseConnector.getDBConnection();
        int countOfRowsUpdated = 0;
        try {
            PreparedStatement ps = connection.prepareStatement(PropertiesReader.getStringFromProperties("CREATE_PHONE"));
            ps.setString(1, model);
            ps.setString(2, number);
            ps.setInt(3, getUserID(name, surname, username, password));
            ps.setInt(4, networkProviderId);
            countOfRowsUpdated = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return countOfRowsUpdated;
    }

    @Override
    public int getUserID(String name, String surname, String username, String password) {
        Connection connection = DatabaseConnector.getDBConnection();
        int userId = 0;
        ResultSet rs = null;
        try {
            PreparedStatement ps = connection.prepareStatement(PropertiesReader.getStringFromProperties("GET_USER_ID"));
            ps.setString(1, name);
            ps.setString(2, surname);
            ps.setString(3, username);
            ps.setString(4, password);
            rs = ps.executeQuery();
            while (rs.next()) {
                userId = rs.getInt("id");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userId;
    }
}
