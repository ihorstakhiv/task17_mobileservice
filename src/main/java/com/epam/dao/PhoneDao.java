package com.epam.dao;

import com.epam.dao.abstractDao.PhoneDaoAbstract;
import com.epam.database.DatabaseConnector;
import com.epam.utils.DatabaseParser;
import com.epam.model.Call;
import com.epam.model.Message;
import com.epam.utils.PropertiesReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PhoneDao implements PhoneDaoAbstract {

    @Override
    public int registerCall(String receiver, int callTime, int userID){
        Connection connection = DatabaseConnector.getDBConnection();
        int countOfRowsUpdated = 0;
        try {
            PreparedStatement ps = connection.prepareStatement(PropertiesReader.getStringFromProperties("ADD_CALL"));
            ps.setString(1, receiver);
            ps.setInt(2, callTime);
            ps.setInt(3, userID);
            countOfRowsUpdated = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return countOfRowsUpdated;
    }

    @Override
    public List<Call> getCallHistory(int userID) throws SQLException {
        List<Call> calls = new ArrayList<>();
        Connection connection = DatabaseConnector.getDBConnection();
        ResultSet rs = null;
        try {
            PreparedStatement ps = connection.prepareStatement(PropertiesReader.getStringFromProperties("GET_CALLS"));
            ps.setInt(1, userID);
            rs = ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        while (rs.next()){
            calls.add((Call) new DatabaseParser(Call.class).fromResultSetToEntity(rs));
        }
        return calls;
    }

    @Override
    public int registerSMS(String receiverNumber, String smsText, int userID) {
        Connection connection = DatabaseConnector.getDBConnection();
        int countOfRowsUpdated = 0;
        try {
            PreparedStatement ps = connection.prepareStatement(PropertiesReader.getStringFromProperties("ADD_SMS"));
            ps.setString(1, receiverNumber);
            ps.setString(2, smsText);
            ps.setInt(3, userID);
            countOfRowsUpdated = ps.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return countOfRowsUpdated;
    }

    @Override
    public List<Message> getSmsHistory(int userID) throws SQLException {
        List<Message> sms = new ArrayList<>();
        Connection connection = DatabaseConnector.getDBConnection();
        ResultSet rs = null;
        try {
            PreparedStatement ps = connection.prepareStatement(PropertiesReader.getStringFromProperties("GET_SMS"));
            ps.setInt(1, userID);
            rs = ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        while (rs.next()) {
            sms.add((Message) new DatabaseParser(Message.class).fromResultSetToEntity(rs));
        }
        return sms;
    }
}
