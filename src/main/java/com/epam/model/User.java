package com.epam.model;

import com.epam.model.annotations.Column;
import com.epam.model.annotations.Table;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Table(name = "user")
public class User {
    public static final Logger logger = LogManager.getLogger(User.class);
    @Column(name = "id")
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "surname")
    private String surname;
    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;

    public User() {
    }

    public User(String name, String surname, String username, String password) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void print() {
        logger.info("\tUSER INFO" +
                "\n------------------------------" +
                "\nID: " + getId() +
                "\nName: " + getName() +
                "\nSurname: " + getSurname() +
                "\nUsername: " + getUsername() +
                "\nPassword: " + getPassword() +
                "\n------------------------------");
    }
}
