package com.epam.model;

import com.epam.model.annotations.Column;
import com.epam.model.annotations.Table;

@Table(name = "phone")
public class Phone {
    @Column(name = "id")
    private int id;
    @Column(name = "com/epam/model")
    private String model;
    @Column(name = "number")
    private String number;
    @Column(name = "balance")
    private double balance;
    @Column(name = "user_id")
    private int user_id;
    @Column(name = "provider_id")
    private int provider_id;

    public Phone() {
    }

    public Phone(String model, String number, int user_id) {
        this.model = model;
        this.number = number;
        this.balance = 0.00;
        this.user_id = user_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getProvider_id() { return provider_id; }

    public void setProvider_id(int provider_id) { this.provider_id = provider_id; }
}
