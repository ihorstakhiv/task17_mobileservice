package com.epam.service;

import com.epam.dao.LoginDao;
import com.epam.dao.PhoneDao;
import com.epam.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.sql.SQLException;
import java.util.stream.Collectors;

public class PhoneServiceTest {
    private User user;
    public PhoneServiceTest() {
        try {
            user = new LoginDao().getUser("test", "test");
        } catch (SQLException e) {
            e.printStackTrace();
            Assertions.fail("wrong sql request");
        }
    }

    @Test
    public void callsHistorySameInListTest() throws SQLException {
        Assertions.assertSame(new PhoneDao().getCallHistory(user.getId()).size(),
                new PhoneDao().getCallHistory(user.getId()).stream()
                        .distinct()
                        .collect(Collectors.toList()).size());
    }

    @Test
    public void smsHistorySameInListTest() throws SQLException {
        Assertions.assertSame(new PhoneDao().getSmsHistory(user.getId()).size(),
                new PhoneDao().getSmsHistory(user.getId()).stream()
                        .distinct()
                        .collect(Collectors.toList()).size());
    }
}
