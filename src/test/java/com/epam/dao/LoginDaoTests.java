package com.epam.dao;

import com.epam.model.User;
import org.junit.jupiter.api.Test;
import java.sql.SQLException;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class LoginDaoTests {
    LoginDaoTests() {
        try {
            new LoginDao().getUser("test", "test");
        } catch (NullPointerException e) {
            new RegisterDao().createUser("test", "test", "test", "test");
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getUserTest() throws SQLException {
        User user = new LoginDao().getUser("test", "test");
        assertNotNull(user);
    }
}