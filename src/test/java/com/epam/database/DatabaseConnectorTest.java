package com.epam.database;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import java.sql.Connection;
import java.sql.SQLException;

public class DatabaseConnectorTest {

        @Test
        public void dbConnectionValidTest() {
            Connection dbConnection = DatabaseConnector.getDBConnection();
            try {
                dbConnection.isValid(5);
            } catch (SQLException e) {
                e.getErrorCode();
                Assertions.fail();
            }
        }

        @Test
        public void dbConnectionNotNullTest()  {
            Connection dbConnection = DatabaseConnector.getDBConnection();
            Assertions.assertNotNull(dbConnection);
        }
    }
